# AT&T Web Scraper

### Assignment
1. Create a web page or GUI that will accept a public URL input from the user (BBC, wikipedia, etc.)
2. Web-scrape articles from the URL
3. Create a web page or GUI showing the scraped articles
4. Change the style of the article so that each letter's height is inversely proportional to its frequency in the particular article e.g. all 'e' letters are likely to be very small whereas all 'z' letters are likely to be very big

### Technical details
- No backend. Application uses only web browser technologies to meet requirements.
- Depends on proxy server, to get around CORS. Currently it's a public one: https://cors-anywhere.herokuapp.com.

### Implementation details
- Implemented in Angular 7.3.0.
- Application uses Google's Material Design components for UI.
- Application uses rx.js for state management.
- Application is responsive to different viewports.
- Scraping is done by ScrapperService.
- Formatting is done by FormatService using `formatLetterFrequency` method. Letter frequencies are by default remapped to scale from 10px to 30px. This is overridable.
- Application is currently hosted at https://att-webscraper.netlify.com/.

### Future improvements
- Remove dependency on public proxy, by hosting our own.
- Higher test coverage.


This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
