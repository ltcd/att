import { FormatService } from './format.service';
import { ScrapperService, Article } from './scrapper.service';
import { LoadingService } from './loading.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  isLoading = false;
  articles: Array<Article>;

  // Subscriptions
  loadingServiceSubscription: Subscription;
  scrapperServiceSubscription: Subscription;

  constructor(
    private loadingService: LoadingService,
    private scrapperService: ScrapperService,
    private formatService: FormatService,
  ) {}

  ngOnInit() {
    this.loadingServiceSubscription =
      this.loadingService
        .getLoading()
        .subscribe(change => this.isLoading = change);

    this.scrapperServiceSubscription =
      this.scrapperService
        .getArticles()
        .subscribe(articles => {
          this.loadingService.setLoading(false);

          this.articles = articles.map(article => ({
            ...article,
            content: this.formatService.formatLetterFrequency(article.content),
          }));
        });
  }

  ngOnDestroy() {
    this.loadingServiceSubscription.unsubscribe();
    this.scrapperServiceSubscription.unsubscribe();
  }
}
