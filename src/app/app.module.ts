import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injectable, ErrorHandler } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';


import {
  MatButtonModule,
  MatCheckboxModule,
  MatToolbarModule,
  MatProgressBarModule,
  MatFormFieldModule,
  MatInputModule,
  MatOptionModule,
  MatSelectModule,
  MatDividerModule,
  MatGridListModule,
  MatCardModule,
  MatExpansionModule,
} from '@angular/material';

import { ScrapperConfiguratorComponent } from './scrapper-configurator/scrapper-configurator.component';
import { ArticleComponent } from './article/article.component';
import { SafeHtmlPipe } from './safe-html.pipe';

import * as Sentry from '@sentry/browser';

Sentry.init({
  dsn: 'https://03cd924af72e4c2fafcf1364863e84bc@sentry.io/1814236'
});

@Injectable()
export class SentryErrorHandler implements ErrorHandler {
  constructor() {}
  handleError(error) {
    const eventId = Sentry.captureException(error.originalError || error);
    Sentry.showReportDialog({ eventId });
  }
}

@NgModule({
  declarations: [
    AppComponent,
    ScrapperConfiguratorComponent,
    ArticleComponent,
    SafeHtmlPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,

    MatToolbarModule,
    MatButtonModule,
    MatCheckboxModule,
    MatProgressBarModule,
    MatFormFieldModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    MatDividerModule,
    MatGridListModule,
    MatCardModule,
    MatExpansionModule,
  ],
  providers: [
    { provide: ErrorHandler, useClass: SentryErrorHandler }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
