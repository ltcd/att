import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FormatService {

  constructor() { }


  private static mapToScale = (
    value: number,
    fromMin: number,
    fromMax: number,
    toMin: number,
    toMax: number
  ): number =>
    toMin + (toMax - toMin) * ((value - fromMin) / (fromMax - fromMin))

  private static histogram = (arr: Array<string>) =>
    arr.reduce((acc, e) => {
      if (e in acc) {
        acc[e]++;
      } else {
        acc[e] = 1;
      }

      return acc;
    }, {})

  private static histogramMinMax(hist: {[s: string]: number}) {
    const vals = Object.values(hist);

    return {
      min: Math.min(...vals),
      max: Math.max(...vals),
    };
  }

  public formatLetterFrequency(str: string, minLetterPx = 10, maxLetterPx = 30): string {
    const charArray = str.split('');

    const hist = FormatService.histogram(charArray);

    const { min, max } = FormatService.histogramMinMax(hist);

    const scaledHist = Object.keys(hist).reduce((acc, key) => {
      acc[key] = FormatService.mapToScale(hist[key], max, min, minLetterPx, maxLetterPx);
      return acc;
    }, {});

    const formatted =
      charArray
        .map(ch => `<span style="font-size: ${scaledHist[ch]}px">${ch}</span>`)
        .join('');

    return formatted;
  }
}
