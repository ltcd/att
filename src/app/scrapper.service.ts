import { Article } from './scrapper.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, BehaviorSubject } from 'rxjs';


type CSSSelector = string;
export type Uri = string;

export interface ArticleSelector {
  articleSelector: CSSSelector;
  titleSelector: CSSSelector;
  contentSelector: CSSSelector;
  authorSelector?: CSSSelector;
  timestampSelector?: CSSSelector;
}

export interface Article {
  title: string;
  content: string;
  author?: string;
  timestamp?: string;
}

@Injectable({
  providedIn: 'root'
})
export class ScrapperService {
  private dom = document.createElement('html');
  private latestArticles: BehaviorSubject<Array<Article>> = new BehaviorSubject([]);

  constructor(private http: HttpClient) { }

  getArticles(): BehaviorSubject<Array<Article>> {
    return this.latestArticles;
  }

  scrap(uri: Uri, as: ArticleSelector): void {
    const PROXY = 'https://cors-anywhere.herokuapp.com/';

    this.http
      .get(PROXY + uri, { responseType: 'text' })
      .subscribe(res => {
        this.dom.innerHTML = res;

        const articles = this.dom.querySelectorAll(as.articleSelector);

        const parsedArticles: [Article?] = [];

        for (const article of articles as any) {
          const parsedArticle: Article = {
            title: article.querySelector(as.titleSelector),
            content: article.querySelector(as.contentSelector),
            author: article.querySelector(as.authorSelector),
            timestamp: article.querySelector(as.timestampSelector),
          };

          // Extract and trim contents. (possibility of "Extract method" in the future)
          for (const key of Object.keys(parsedArticle)) {
            parsedArticle[key] = parsedArticle[key] ? parsedArticle[key].innerText.trim() : null;
          }

          // discard articles without title and content
          if ([parsedArticle.title, parsedArticle.content].some(attr => !attr)) {
            continue;
          }

          parsedArticles.push(parsedArticle);
        }

        this.latestArticles.next(parsedArticles);
      });
  }
}
