import { Component, OnInit, Input } from '@angular/core';
import { Article } from './../scrapper.service';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {
  @Input() articleData: Article;

  constructor() { }

  ngOnInit() {

  }

}
