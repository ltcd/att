import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  private isLoading: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor() { }

  setLoading(isLoading: boolean): void {
    this.isLoading.next(isLoading);
  }

  getLoading(): BehaviorSubject<boolean> {
    return this.isLoading;
  }
}
