import { LoadingService } from './../loading.service';
import { ScrapperService, ArticleSelector, Uri } from './../scrapper.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, ValidatorFn, AbstractControl } from '@angular/forms';



export function urlValidator(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    const valid = new RegExp(
      '^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$'
      ).test(control.value);
    return !valid ? {invalidUrl: {value: control.value}} : null;
  };
}
export function cssSelectorValidator(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    if (!control.value) {
      return null;
    }

    let invalid = false;

    try {
      document.querySelector(control.value);
    } catch (err) {
      invalid = true;
    }
    return invalid ? {invalidCSSSelector: {value: control.value}} : null;
  };
}


@Component({
  selector: 'app-scrapper-configurator',
  templateUrl: './scrapper-configurator.component.html',
  styleUrls: ['./scrapper-configurator.component.css']
})
export class ScrapperConfiguratorComponent implements OnInit {
  readonly presets: {[k: string]: ArticleSelector&{uriToScrap: Uri} } = {
    None: {
      uriToScrap: '',
      articleSelector: '',
      titleSelector: '',
      contentSelector: '',
      authorSelector: '',
      timestampSelector: '',
    },
    LifeHacker: {
      uriToScrap: 'https://lifehacker.com',
      articleSelector: '.postlist__item',
      titleSelector: '.headline a',
      contentSelector: '.excerpt',
      authorSelector: '.author',
      timestampSelector: '.js_publish_time',
    },
    TechCrunch: {
      uriToScrap: 'https://techcrunch.com',
      articleSelector: '.post-block',
      titleSelector: '.post-block__title a',
      contentSelector: '.post-block__content',
      authorSelector: '.river-byline__authors a',
      timestampSelector: '.river-byline__time',
    },
    'sme.sk': {
      uriToScrap: 'https://domov.sme.sk',
      articleSelector: '.media-body',
      titleSelector: '.media-heading a',
      contentSelector: '.js-pvt-perex',
      authorSelector: null,
      timestampSelector: null,
    },
    'aktuality.sk': {
      uriToScrap: 'https://www.aktuality.sk',
      articleSelector: '.article-item',
      titleSelector: '.article-title',
      contentSelector: '.article-perex',
      authorSelector: null,
      timestampSelector: null,
    },
    BBC: {
      uriToScrap: 'https://www.bbc.com',
      articleSelector: '.media-list__item',
      titleSelector: '.media__title a',
      contentSelector: '.media__summary',
      authorSelector: null,
      timestampSelector: null,
    },
  };

  readonly presetList = Object.keys(this.presets);

  isExpanded = true;

  scrapperConfigurationForm = new FormGroup({
    // activePreset: new FormControl(''),
    uriToScrap: new FormControl('', [urlValidator()]),
    articleSelector: new FormControl('', [cssSelectorValidator()]),
    titleSelector: new FormControl('', [cssSelectorValidator()]),
    contentSelector: new FormControl('', [cssSelectorValidator()]),
    authorSelector: new FormControl('', [cssSelectorValidator()]),
    timestampSelector: new FormControl('', [cssSelectorValidator()]),
  });

  constructor(private scrapperService: ScrapperService, private loadingService: LoadingService) { }

  ngOnInit() {
  }

  submit() {
    this.isExpanded = false;
    this.loadingService.setLoading(true);
    this.scrapperService.scrap(this.scrapperConfigurationForm.get('uriToScrap').value, {
      articleSelector: this.scrapperConfigurationForm.get('articleSelector').value,
      titleSelector: this.scrapperConfigurationForm.get('titleSelector').value,
      contentSelector: this.scrapperConfigurationForm.get('contentSelector').value,
      authorSelector: this.scrapperConfigurationForm.get('authorSelector').value,
      timestampSelector: this.scrapperConfigurationForm.get('timestampSelector').value,
    });
  }

  presetChange(selectedPreset: string) {
    this.scrapperConfigurationForm.patchValue(this.presets[selectedPreset]);
  }

}
