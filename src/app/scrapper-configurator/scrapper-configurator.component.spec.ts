import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScrapperConfiguratorComponent } from './scrapper-configurator.component';

describe('ScrapperConfiguratorComponent', () => {
  let component: ScrapperConfiguratorComponent;
  let fixture: ComponentFixture<ScrapperConfiguratorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScrapperConfiguratorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScrapperConfiguratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
